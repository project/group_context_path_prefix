<?php

declare(strict_types=1);

namespace Drupal\Tests\group_context_path_prefix\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\group_context_path_prefix\Trait\PathPrefixMatcherTrait;

/**
 * Tests the PathPrefixMatcherTrait.
 *
 * @group group_context_path_prefix
 */
final class PathPrefixMatcherTraitTest extends UnitTestCase {

  use PathPrefixMatcherTrait;

  /**
   * Tests the matching logic in the PathPrefixMatcherTrait.
   *
   * @param string $path
   *   The path to match.
   * @param array<mixed, string> $prefixes
   *   The possible prefixes.
   * @param int|string|false $expect_key
   *   The expected result.
   *
   * @dataProvider getTestCases
   */
  public function testMatchPathPrefix(string $path, array $prefixes, int|string|FALSE $expect_key): void {
    $actual = self::matchPathPrefix($path, $prefixes);
    self::assertSame($expect_key, $actual);
  }

  /**
   * Provides #allthecases.
   */
  public function getTestCases(): array {
    return [
      'no match' => [
        '/foo',
        [],
        FALSE,
      ],
      'exact match' => [
        '/foo',
        ['/foo'],
        0,
      ],
      'no substring match' => [
        '/foobar',
        ['/foo'],
        FALSE,
      ],
      'multi-segment request path, match' => [
        '/foo/bar',
        ['/foo'],
        0,
      ],
      'multi-segment request path, no match' => [
        '/foo/bar',
        ['/bar'],
        FALSE,
      ],
      'multiple prefixes, no match' => [
        '/foo',
        ['/bar', '/baz'],
        FALSE,
      ],
      'multiple prefixes, exact match' => [
        '/foo',
        ['/foo', '/foo/bar'],
        0,
      ],
      'multiple prefixes, exact match, reversed' => [
        '/foo',
        ['/foo/bar', '/foo'],
        1,
      ],
      'multiple prefixes, no substring match' => [
        '/foobar',
        ['/foo', '/bar'],
        FALSE,
      ],
      'multiple prefixes, multi-segment request path, match' => [
        '/foo/bar',
        ['/foo', '/foo/bar'],
        1,
      ],
      'multiple prefixes, multi-segment request path, match, reversed' => [
        '/foo/bar',
        ['/foo/bar', '/foo'],
        0,
      ],
      'multiple prefixes, multi-segment request path, no match' => [
        '/foo/bar',
        ['/bar', '/baz'],
        FALSE,
      ],
    ];
  }

}
