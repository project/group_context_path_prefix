<?php

declare(strict_types=1);

namespace Drupal\Tests\group_context_path_prefix\Kernel;

use Drupal\Core\Plugin\Context\ContextRepositoryInterface;
use Drupal\Tests\group\Kernel\GroupKernelTestBase;

/**
 * Tests the GroupContextPathPrefix class.
 */
final class GroupContextPathPrefixTest extends GroupKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'group_context_path_prefix',
  ];

  /**
   * Tests that the context plugin is registered.
   */
  public function testPluginRegistration(): void {
    $context_repository = $this->container->get('context.repository');
    \assert($context_repository instanceof ContextRepositoryInterface);
    $contexts = $context_repository->getAvailableContexts();
    self::assertArrayHasKey('@group_context_path_prefix.path_prefix_context:group', $contexts);
    $group_context = $contexts['@group_context_path_prefix.path_prefix_context:group'];
    self::assertEquals('Group from URL path prefix', (string) $group_context->getContextDefinition()->getLabel());
  }

}
