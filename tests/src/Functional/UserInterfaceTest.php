<?php

declare(strict_types=1);

namespace Drupal\Tests\group_context_path_prefix\Functional;

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\Tests\group\Functional\GroupBrowserTestBase;
use Drupal\Tests\user\Traits\UserCreationTrait;
use Drupal\group\Entity\GroupTypeInterface;
use Drupal\group\PermissionScopeInterface;
use Drupal\group_context_path_prefix\Entity\GroupPathPrefix;
use Drupal\user\RoleInterface;

/**
 * Tests this module's user interface.
 *
 * @group group_context_path_prefix
 */
final class UserInterfaceTest extends GroupBrowserTestBase {

  use UserCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'group_context_path_prefix',
  ];

  /**
   * A test group type.
   */
  private GroupTypeInterface $groupType;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    // Set up a group type and group role.
    $this->groupType = $this->createGroupType(['creator_membership' => FALSE]);
    $this->createGroupRole([
      'group_type' => $this->groupType->id(),
      'scope' => PermissionScopeInterface::OUTSIDER_ID,
      'permissions' => ['view group'],
      'global_role' => RoleInterface::AUTHENTICATED_ID,
    ]);
  }

  /**
   * Tests that the 'Group from URL path prefix' option is available.
   */
  public function testPathPrefixUi(): void {
    $assert = $this->assertSession();
    // Anonymous can't access.
    $path_prefix_overview = Url::fromRoute('group_context_path_prefix.overview');
    $this->drupalGet($path_prefix_overview);
    $assert->pageTextContains('Access denied');
    // Log in as group admin.
    $auth_user = $this->drupalCreateUser(['administer group']);
    \assert($auth_user instanceof AccountInterface);
    $this->drupalLogin($auth_user);
    $path_prefix_overview = Url::fromRoute('group_context_path_prefix.overview');
    $this->drupalGet($path_prefix_overview);
    // Test before any groups have been created.
    $assert->pageTextContains('There are no groups yet.');
    // Create a test group.
    $group = $this->createGroup(['type' => $this->groupType->id()]);
    // Test that the new group does not have a prefix defined.
    $this->drupalGet($path_prefix_overview);
    $assert->pageTextContains((string) $group->label());
    $assert->linkExists('Edit path');
    $assert->linkByHrefExists($group->toUrl('edit-path-prefix-form')->toString());
    // Update the group's path prefix by editing the group path.
    $assert->pageTextContains('Not defined');
    $this->clickLink('Edit path');
    // Submit the edit form.
    $this->submitForm([
      GroupPathPrefix::FIELD_NAME => '/a/b',
    ], 'Save');
    $assert->pageTextContains(\sprintf('%s path prefix updated.', $group->label()));
    $assert->pageTextContains('/a/b');
    // Create a second test group.
    $group_2 = $this->createGroup(['type' => $this->groupType->id()]);
    // Edit the its path prefix.
    $this->drupalGet($group_2->toUrl('edit-path-prefix-form'));
    // Submit the edit form with a duplicate path alias.
    $this->submitForm([
      GroupPathPrefix::FIELD_NAME => '/a/b',
    ], 'Save');
    $assert->pageTextNotContains(\sprintf('%s path prefix updated.', $group_2->label()));
    $assert->pageTextContains('A group with Path /a/b already exists.');
    // Submit the edit form with an invalid path prefix.
    $this->submitForm([
      GroupPathPrefix::FIELD_NAME => '/a/',
    ], 'Save');
    $assert->pageTextNotContains(\sprintf('%s path prefix updated.', $group_2->label()));
    $assert->pageTextContains('/a/ is not a valid URL path prefix.');
    // Finally, submit the edit form with a valid path prefix.
    $this->submitForm([
      GroupPathPrefix::FIELD_NAME => '/x/y/z',
    ], 'Save');
    $assert->pageTextContains(\sprintf('%s path prefix updated.', $group_2->label()));
    $assert->pageTextContains('/a/b');
    $assert->pageTextContains('/x/y/z');
  }

  /**
   * Tests the edit path prefix entity operation.
   */
  public function testEditPathPrefixGroupOperation(): void {
    $assert = $this->assertSession();
    // Log in as group admin.
    $auth_user = $this->drupalCreateUser([], NULL, TRUE);
    \assert($auth_user instanceof AccountInterface);
    $this->drupalLogin($auth_user);
    // Create a test group.
    $group = $this->createGroup(['type' => $this->groupType->id()]);
    // Visit the group list.
    $this->drupalGet(Url::fromRoute('entity.group.collection'));
    // Ensure the expected group appears.
    $assert->pageTextContains($group->label());
    // Ensure the edit path prefix entity operation appears on the group
    // collection page.
    $assert->linkExists('Path prefix');
  }

}
