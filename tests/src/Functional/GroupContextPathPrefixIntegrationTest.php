<?php

declare(strict_types=1);

namespace Drupal\Tests\group_context_path_prefix\Functional;

use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Url;
use Drupal\Tests\group\Functional\GroupBrowserTestBase;
use Drupal\Tests\language\Traits\LanguageTestTrait;
use Drupal\group\Entity\GroupInterface;
use Drupal\group\PermissionScopeInterface;
use Drupal\group\Plugin\Group\RelationHandler\PermissionProviderInterface;
use Drupal\group_context_path_prefix\Entity\GroupPathPrefix;
use Drupal\group_context_path_prefix\PathProcessor\GroupPathPrefixProcessor;
use Drupal\group_context_path_prefix\Plugin\LanguageNegotiation\GroupPrefixLanguageNegotiationUrl;
use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\node\NodeInterface;
use Drupal\user\RoleInterface;
use Drupal\user\UserInterface;

/**
 * High-level integration test of the module.
 *
 * Tests the various functions of the module when integrated together and with
 * features of the group, group_sites, and gnode module.
 *
 * @group group_context_path_prefix
 */
final class GroupContextPathPrefixIntegrationTest extends GroupBrowserTestBase {

  use LanguageTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'content_translation',
    'gnode',
    'group',
    'group_context_path_prefix',
    'group_sites',
    'language',
    'redirect',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The test user.
   */
  private readonly UserInterface $testUser;

  /**
   * A test group.
   */
  private readonly GroupInterface $groupA;

  /**
   * Another test group.
   */
  private readonly GroupInterface $groupB;

  /**
   * A test node.
   */
  private readonly NodeInterface $nodeA;

  /**
   * Another test node.
   */
  private readonly NodeInterface $nodeB;

  /**
   * Another test node.
   */
  private readonly NodeInterface $nodeC;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    // Create a test group type.
    $groupType = $this->createGroupType(['creator_membership' => FALSE]);

    // Create a group admin.
    $this->testUser = $this->drupalCreateUser([
      'access content overview',
      'access group overview',
      'use group_sites admin mode',
    ]);
    // Create test content type.
    $nodeType = $this->drupalCreateContentType([
      'type' => 'article',
      'name' => 'Article',
    ]);
    // Enable the gnode content plugin.
    $groupPluginId = "group_node:{$nodeType->id()}";
    $this->entityTypeManager->getStorage('group_relationship_type')
      ->createFromPlugin($groupType, $groupPluginId)->save();
    // Set up a group role.
    $node_permission_provider = $this->container->get('group_relation_type.manager')
      ->getPermissionProvider('group_node:article');
    \assert($node_permission_provider instanceof PermissionProviderInterface);
    $outsider_base = [
      'group_type' => $groupType->id(),
      'scope' => PermissionScopeInterface::OUTSIDER_ID,
      'permissions' => [
        'view group',
        $node_permission_provider->getPermission('view', 'entity'),
      ],
    ];
    $this->createGroupRole(['global_role' => RoleInterface::AUTHENTICATED_ID] + $outsider_base);
    $this->config('group_sites.settings')
      ->set('no_site_access_policy', 'group_sites.no_site_access_policy.do_nothing')
      ->save();

    // Add a few languages.
    ConfigurableLanguage::createFromLangcode('es')->save();
    ConfigurableLanguage::createFromLangcode('de')->save();

    $config = $this->config('language.negotiation');
    $config->set('url.prefixes', ['en' => 'en', 'es' => 'es', 'de' => 'de'])
      ->save();
    $config = $this->config('language.types');
    $config->set('configurable', [LanguageInterface::TYPE_INTERFACE]);
    $config->set('negotiation.language_interface.enabled', [
      GroupPrefixLanguageNegotiationUrl::METHOD_ID => 0,
    ]);
    $config->save();
    $this->container->get('content_translation.manager')->setEnabled('group', $groupType->id(), TRUE);
    $this->container->get('content_translation.manager')->setEnabled('node', $nodeType->id(), TRUE);
    self::enableBundleTranslation('group', $groupType->id());
    self::enableBundleTranslation('node', $nodeType->id());
    // Rebuild the container so that the new languages are picked up by services
    // that hold a list of languages.
    $this->rebuildContainer();

    // Create test groups.
    $this->groupA = $this->createGroup([
      'type' => $groupType->id(),
      'group_context_path_prefix' => '/' . $this->randomMachineName(),
      'label' => 'Group A',
      'uid' => $this->testUser->id(),
    ]);
    $this->groupB = $this->createGroup([
      'type' => $groupType->id(),
      'group_context_path_prefix' => '/' . $this->randomMachineName(),
      'label' => 'Group B',
      'uid' => $this->testUser->id(),
    ]);

    // Create three test nodes.
    $this->nodeA = $this->drupalCreateNode([
      'type' => $nodeType->id(),
      'uid' => $this->testUser->id(),
      'status' => 1,
      'title' => 'Node A',
    ]);
    $this->nodeB = $this->drupalCreateNode([
      'type' => $nodeType->id(),
      'uid' => $this->testUser->id(),
      'status' => 1,
      'title' => 'Node B',
    ]);
    $this->nodeC = $this->drupalCreateNode([
      'type' => $nodeType->id(),
      'uid' => $this->testUser->id(),
      'status' => 1,
      'title' => 'Node C',
    ]);
    // Place each node in its respective group.
    $this->groupA->addRelationship($this->nodeA, $groupPluginId);
    $this->groupB->addRelationship($this->nodeB, $groupPluginId);
    $this->groupA->addRelationship($this->nodeC, $groupPluginId);
    $this->groupB->addRelationship($this->nodeC, $groupPluginId);
    // Enable the group_context_path_prefix.path_prefix_context service with the
    // group_sites module.
    $group_sites_settings = $this->config('group_sites.settings');
    $group_sites_settings->set('context_provider', '@group_context_path_prefix.path_prefix_context:group');
    $group_sites_settings->save();
  }

  /**
   * Tests this module in integration with a monolingual site.
   */
  public function testIntegrationMonolingual(): void {
    ConfigurableLanguage::load('es')->delete();
    ConfigurableLanguage::load('de')->delete();
    // Rebuild the container so that a monolingual site is identified.
    $this->rebuildContainer();

    $assert = $this->assertSession();
    $this->drupalLogin($this->testUser);
    // Visit node A via the group A prefix.
    $group_a_path_prefix = GroupPathPrefix::get($this->groupA);
    $node_a_id = $this->nodeA->id();
    $url = Url::fromUri("base:{$group_a_path_prefix}/node/{$node_a_id}", ['absolute' => TRUE]);
    $this->assertEquals("{$this->baseUrl}{$group_a_path_prefix}/node/{$node_a_id}", $url->toString());
    $this->drupalGet($url);
    $assert->pageTextContains($this->nodeA->label());
    // Visit node B via the group B prefix.
    $group_b_path_prefix = GroupPathPrefix::get($this->groupB);
    $node_b_id = $this->nodeB->id();
    $url = Url::fromUri("base:{$group_b_path_prefix}/node/{$node_b_id}", ['absolute' => TRUE]);
    $this->assertEquals("{$this->baseUrl}{$group_b_path_prefix}/node/{$node_b_id}", $url->toString());
    $this->drupalGet($url);
    $assert->pageTextContains($this->nodeB->label());
    // Visit node A, using the group B path prefix.
    $url = Url::fromUri("base:{$group_b_path_prefix}/node/{$node_a_id}", ['absolute' => TRUE]);
    $this->assertEquals("{$this->baseUrl}{$group_b_path_prefix}/node/{$node_a_id}", $url->toString());
    $this->drupalGet($url);
    // Should have a 403 since the page does not exist in group B.
    $assert->statusCodeEquals(403);
    self::assertSame("{$this->baseUrl}{$group_b_path_prefix}/node/{$node_a_id}", $this->getSession()->getCurrentUrl());
    // Visit node C via the group A & B prefix.
    $node_c_id = $this->nodeC->id();
    $url = Url::fromUri("base:{$group_a_path_prefix}/node/{$node_c_id}", ['absolute' => TRUE]);
    $this->assertEquals("{$this->baseUrl}{$group_a_path_prefix}/node/{$node_c_id}", $url->toString());
    $this->drupalGet($url);
    $assert->pageTextContains($this->nodeC->label());
    self::assertSame("{$this->baseUrl}{$group_a_path_prefix}/node/{$node_c_id}", $this->getSession()->getCurrentUrl());
    $url = Url::fromUri("base:{$group_b_path_prefix}/node/{$node_c_id}", ['absolute' => TRUE]);
    $this->assertEquals("{$this->baseUrl}{$group_b_path_prefix}/node/{$node_c_id}", $url->toString());
    $this->drupalGet($url);
    $assert->pageTextContains($this->nodeC->label());
    self::assertSame("{$this->baseUrl}{$group_b_path_prefix}/node/{$node_c_id}", $this->getSession()->getCurrentUrl());
    // Visit /{groupA}/admin/content; only node A should appear.
    $this->drupalGet("{$this->baseUrl}{$group_a_path_prefix}/admin/content");
    $assert->statusCodeEquals(200);
    $assert->pageTextContains($this->nodeA->label());
    $assert->pageTextNotContains($this->nodeB->label());
    // Vice-versa.
    $this->drupalGet("{$this->baseUrl}{$group_b_path_prefix}/admin/content");
    $assert->statusCodeEquals(200);
    $assert->pageTextNotContains($this->nodeA->label());
    $assert->pageTextContains($this->nodeB->label());
    // The group canonical page should use its prefix.
    $group_canonical_path = $this->groupA->toUrl(NULL, ['absolute' => TRUE]);
    self::assertSame("{$this->baseUrl}{$group_a_path_prefix}/group/{$this->groupA->id()}", $group_canonical_path->toString());
    $this->drupalGet($group_canonical_path);
    self::assertSame("{$this->baseUrl}{$group_a_path_prefix}/group/{$this->groupA->id()}", $this->getSession()->getCurrentUrl());
    $assert->pageTextContains($this->groupA->label());
    // Set the front page to a group with a prefix.
    $this->config('system.site')->set('page.front', "/group/{$this->groupA->id()}")->save();
    $this->drupalGet(Url::fromRoute('<front>'));
    self::assertSame("{$this->baseUrl}{$group_a_path_prefix}", $this->getSession()->getCurrentUrl());
    $assert->statusCodeEquals(200);
    $assert->pageTextContains($this->groupA->label());
    // Visit the group B site.
    $url = Url::fromUri("base:{$group_b_path_prefix}", ['absolute' => TRUE]);
    $this->assertEquals("{$this->baseUrl}{$group_b_path_prefix}", $url->toString());
    $this->drupalGet($url);
    $assert->statusCodeEquals(200);
    $assert->pageTextContains($this->groupB->label());
  }

  /**
   * Tests this module in integration with a multilingual site.
   */
  public function testIntegrationMultilingual(): void {
    $english = ConfigurableLanguage::load('en');
    self::assertInstanceOf(ConfigurableLanguage::class, $english);
    $assert = $this->assertSession();
    $this->drupalLogin($this->testUser);
    // Visit node A via the group A prefix.
    $group_a_path_prefix = GroupPathPrefix::get($this->groupA);
    $node_a_id = $this->nodeA->id();
    $url = Url::fromUri("base:{$group_a_path_prefix}/node/{$node_a_id}", ['absolute' => TRUE]);
    $this->assertEquals("{$this->baseUrl}{$group_a_path_prefix}/node/{$node_a_id}", $url->toString());
    $this->drupalGet($url);
    $assert->pageTextContains($this->nodeA->label());
    // Visit node B via the group B prefix.
    $group_b_path_prefix = GroupPathPrefix::get($this->groupB);
    $node_b_id = $this->nodeB->id();
    $url = Url::fromUri("base:{$group_b_path_prefix}/node/{$node_b_id}", ['absolute' => TRUE]);
    $this->assertEquals("{$this->baseUrl}{$group_b_path_prefix}/node/{$node_b_id}", $url->toString());
    $this->drupalGet($url);
    $assert->pageTextContains($this->nodeB->label());
    // Visit node A, using the group B path prefix.
    $url = Url::fromUri("base:{$group_b_path_prefix}/node/{$node_a_id}", ['absolute' => TRUE]);
    $this->assertEquals("{$this->baseUrl}{$group_b_path_prefix}/node/{$node_a_id}", $url->toString());
    $this->drupalGet($url);
    // Should have a 403 since the page does not exist in group B.
    $assert->statusCodeEquals(403);
    self::assertSame("{$this->baseUrl}{$group_b_path_prefix}/node/{$node_a_id}", $this->getSession()->getCurrentUrl());
    // Visit /{groupA}/admin/content; only node A should appear.
    $url = Url::fromUri('internal:/admin/content', [
      'language' => $english,
      'absolute' => TRUE,
      GroupPathPrefixProcessor::LOCALIZED_OPTIONS_KEY => ['group' => $this->groupA],
    ]);
    $this->assertEquals("{$this->baseUrl}{$group_a_path_prefix}/en/admin/content", $url->toString());
    $this->drupalGet($url);
    $assert->pageTextContains($this->nodeA->label());
    $assert->pageTextNotContains($this->nodeB->label());
    // Vice-versa.
    $url = Url::fromRoute('system.admin_content', [], [
      'language' => $english,
      'absolute' => TRUE,
      GroupPathPrefixProcessor::LOCALIZED_OPTIONS_KEY => ['group' => $this->groupB],
    ]);
    $this->assertEquals("{$this->baseUrl}{$group_b_path_prefix}/en/admin/content", $url->toString());
    $this->drupalGet($url);
    $assert->statusCodeEquals(200);
    $assert->pageTextNotContains($this->nodeA->label());
    $assert->pageTextContains($this->nodeB->label());
    // The group canonical page should use its prefix.
    $group_canonical_path = $this->groupA->toUrl(NULL, ['absolute' => TRUE]);
    self::assertSame("{$this->baseUrl}{$group_a_path_prefix}/en/group/{$this->groupA->id()}", $group_canonical_path->toString());
    $this->drupalGet($group_canonical_path);
    $assert->statusCodeEquals(200);
    self::assertSame("{$this->baseUrl}{$group_a_path_prefix}/en/group/{$this->groupA->id()}", $this->getSession()->getCurrentUrl());
    $assert->pageTextContains($this->groupA->label());
    // Set the front page to a group with a prefix.
    $this->config('system.site')->set('page.front', "/group/{$this->groupA->id()}")->save();
    $this->drupalGet(Url::fromRoute('<front>'));
    self::assertSame("{$this->baseUrl}{$group_a_path_prefix}/en", $this->getSession()->getCurrentUrl());
    $assert->statusCodeEquals(200);
    $assert->pageTextContains($this->groupA->label());
    // Visit the group B site.
    $url = Url::fromUri("base:{$group_b_path_prefix}", ['absolute' => TRUE]);
    $this->assertEquals("{$this->baseUrl}{$group_b_path_prefix}", $url->toString());
    $this->drupalGet($url);
    $assert->statusCodeEquals(200);
    $assert->pageTextContains($this->groupB->label());
    // Add translations to a node.
    $spanish = ConfigurableLanguage::load('es');
    $german = ConfigurableLanguage::load('de');
    $this->nodeA->addTranslation($spanish->getId(), ['title' => 'ES - ' . $this->nodeA->label()])
      ->save();
    $url = Url::fromRoute('entity.node.canonical', ['node' => $node_a_id], [
      'language' => $spanish,
      'absolute' => TRUE,
      GroupPathPrefixProcessor::LOCALIZED_OPTIONS_KEY => ['group' => $this->groupA],
    ]);
    // The URL path should contain the Spanish language prefix since it was
    // specified as a URL option above.
    $this->assertEquals("{$this->baseUrl}{$group_a_path_prefix}/{$spanish->getId()}/node/{$node_a_id}", $url->toString());
    $this->drupalGet($url);
    $assert->statusCodeEquals(200);
    self::assertSame("{$this->baseUrl}{$group_a_path_prefix}/{$spanish->getId()}/node/{$node_a_id}", $this->getSession()->getCurrentUrl());
    $assert->pageTextContains('ES - ' . $this->nodeA->label());
    // Check a German group.
    $this->groupA->addTranslation($german->getId(), ['title' => 'DE - ' . $this->groupA->label()])
      ->save();
    $url = Url::fromUri("internal:{$group_a_path_prefix}/group/{$this->groupA->id()}", [
      'language' => $german,
      'absolute' => TRUE,
    ]);
    // The URL path should contain the German language prefix since it was
    // specified as a URL option above.
    $this->assertEquals("$this->baseUrl{$group_a_path_prefix}/{$german->getId()}", $url->toString());
    $this->drupalGet($url);
    self::assertSame("{$this->baseUrl}{$group_a_path_prefix}/{$german->getId()}", $this->getSession()->getCurrentUrl());
    $assert->statusCodeEquals(200);
    // Visit node C via the group A & B prefix.
    $this->nodeC->addTranslation($spanish->getId(), ['title' => 'ES - ' . $this->nodeC->label()])
      ->save();
    $node_c_id = $this->nodeC->id();
    $url = Url::fromUri("internal:{$group_a_path_prefix}/node/{$node_c_id}", [
      'language' => $spanish,
      'absolute' => TRUE,
    ]);
    $this->assertEquals("{$this->baseUrl}{$group_a_path_prefix}/{$spanish->getId()}/node/{$node_c_id}", $url->toString());
    $this->drupalGet($url);
    self::assertSame("{$this->baseUrl}{$group_a_path_prefix}/{$spanish->getId()}/node/{$node_c_id}", $this->getSession()->getCurrentUrl());
    $assert->pageTextContains($this->nodeC->label());
    $url = Url::fromUri("{$this->baseUrl}{$group_b_path_prefix}/{$spanish->getId()}/node/{$node_c_id}", [
      'language' => $spanish,
      'absolute' => TRUE,
    ]);
    $this->assertEquals("{$this->baseUrl}{$group_b_path_prefix}/{$spanish->getId()}/node/{$node_c_id}", $url->toString());
    $this->drupalGet($url);
    self::assertSame("{$this->baseUrl}{$group_b_path_prefix}/{$spanish->getId()}/node/{$node_c_id}", $this->getSession()->getCurrentUrl());
    $assert->pageTextContains($this->nodeC->label());
    // Verify group URL for an operation URL.
    $url = $this->groupB->toUrl('edit-form', [
      'language' => $english,
      'absolute' => TRUE,
    ]);
    $this->assertEquals("{$this->baseUrl}{$group_b_path_prefix}/{$english->getId()}/group/{$this->groupB->id()}/edit", $url->toString());
    // Verify group URLs in listing pages.
    $url = Url::fromRoute('entity.group.collection');
    $this->container->get('group_sites.admin_mode')->setAdminMode(TRUE);
    $this->drupalGet($url);
    $this->container->get('group_sites.admin_mode')->setAdminMode(FALSE);
    self::assertSame("{$this->baseUrl}{$group_a_path_prefix}/{$english->getId()}/admin/group", $this->getSession()->getCurrentUrl());
    $assert->linkExists($this->groupA->label());
    $assert->linkExists($this->groupB->label());
    $url = $this->groupA->toUrl(NULL, [
      'language' => $english,
    ]);
    $assert->linkByHrefExistsExact($url->toString());
    $this->assertEquals("{$this->baseUrl}$group_a_path_prefix/{$english->getId()}", $url->setAbsolute()->toString());
    $url = $this->groupB->toUrl(NULL, [
      'language' => $english,
    ]);
    $assert->linkByHrefExistsExact($url->toString());
    $this->assertEquals("{$this->baseUrl}$group_b_path_prefix/{$english->getId()}", $url->setAbsolute()->toString());
  }

  /**
   * Tests the group path prefix when it matches a language code path prefix.
   */
  public function testIntegrationMultilingualMatchingPrefix(): void {
    $assert = $this->assertSession();
    $this->drupalLogin($this->testUser);
    // Load multiple languages.
    $german = ConfigurableLanguage::load('de');
    \assert($german, LanguageInterface::class);
    // Publish the test group.
    $this->groupA->setPublished()->save();
    // Create distinguishable labels per-language.
    $germanLabel = 'DE - ' . $this->groupA->label();
    $this->groupA->addTranslation($german->getId(), ['label' => $germanLabel])->save();
    // Set the group path prefix to "germany".
    $this->groupA->set('group_context_path_prefix', '/' . 'germany')->save();
    // Set the front page to the other test group.
    $this->config('system.site')->set('page.front', "/group/{$this->groupB->id()}")->save();
    // Get the group URL for german.
    $germanyGroupUrl = $this->groupA->toUrl('canonical', [
      'group' => $this->groupA,
      'language' => $german,
    ])->setAbsolute();
    self::assertSame($this->baseUrl . '/germany/de', $germanyGroupUrl->toString());
    // GET /germany/de, i.e., the Germany homepage.
    $this->drupalGet($germanyGroupUrl);
    self::assertSame($germanyGroupUrl->toString(), $this->getSession()->getCurrentUrl());
    $assert->pageTextContains($germanLabel);
    // GET /de/de, which should not be found since the Germany homepage has
    // "/germany" for its path prefix.
    $this->drupalGet(Url::fromUri('base:/de/de'));
    self::assertSame($this->baseUrl . '/de/de', $this->getSession()->getCurrentUrl());
    $assert->pageTextContains('Page not found');
    // Set the group path prefix to "germany".
    $this->groupA->set('group_context_path_prefix', '/de')->save();
    // GET /de/de again. The page should be found since the Germany homepage now
    // has "/de" for its path prefix.
    $this->drupalGet(Url::fromUri('base:/de/de'));
    self::assertSame($this->baseUrl . '/de/de', $this->getSession()->getCurrentUrl());
    $assert->pageTextContains($germanLabel);
  }

}
