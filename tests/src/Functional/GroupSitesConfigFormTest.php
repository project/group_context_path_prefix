<?php

declare(strict_types=1);

namespace Drupal\Tests\group_context_path_prefix\Functional;

use Drupal\Core\Url;
use Drupal\Tests\group\Functional\GroupBrowserTestBase;

/**
 * Tests that this module's context can be configured with Group Sites.
 *
 * @group group_context_path_prefix
 */
final class GroupSitesConfigFormTest extends GroupBrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'group_context_path_prefix',
    'group_sites',
  ];

  /**
   * Tests that the 'Group from URL path prefix' option is available.
   */
  public function testPluginRegistration(): void {
    $assert = $this->assertSession();
    $config_form = Url::fromRoute('group_sites.settings');
    $this->drupalLogin($this->rootUser);
    $this->drupalGet($config_form);
    $assert->pageTextContains('Group from URL path prefix');
  }

}
