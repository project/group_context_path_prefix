## INTRODUCTION

This module extends the group module's functionality such that groups can be assigned a URL path prefix.

See the [description on Drupal.org](https://www.drupal.org/project/group_context_path_prefix) for a full summary of the module's features.

## REQUIREMENTS

- [Group](https://www.drupal.org/project/group)

## RECOMMENDED MODULES

- [Group sites](https://www.drupal.org/project/group_sites)

For a multilingual site, site builders will need to use `Group Prefix URL`
for language negotiation and URL processing.  Drupal Core's `URL` assumes
the langcode appears first (langcode/site-prefix/site-page).  But
this modules assumes, many sites will want to reverse that
(site-prefix/langcode/site-page). The Group Prefix URL negotiation makes
this happen. Do not configure Drupal Core's or there will be conflicts.
