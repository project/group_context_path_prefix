<?php

declare(strict_types=1);

namespace Drupal\group_context_path_prefix\Entity\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityConstraintViolationListInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\group\Entity\GroupInterface;
use Drupal\group_context_path_prefix\Entity\GroupPathPrefix;
use Drupal\group_context_path_prefix\Plugin\Validation\Constraint\ValidGroupContextPathPrefixConstraintValidator;

/**
 * Provides a form to edit a group's path prefix.
 */
final class GroupPathPrefixForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  protected $operation = 'edit-path-prefix';

  /**
   * Title callback.
   *
   * @param \Drupal\group\Entity\GroupInterface $group
   *   The edited group.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The form's page title.
   */
  public function title(GroupInterface $group): TranslatableMarkup {
    return $this->t('@label path prefix', [
      '@label' => $group->label(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditedFieldNames(FormStateInterface $form_state) {
    return [GroupPathPrefix::FIELD_NAME];
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form['#process'][] = '::processForm';
    $form['#after_build'][] = '::afterBuild';
    \assert($this->entity instanceof GroupInterface);
    $form[GroupPathPrefix::FIELD_NAME] = [
      '#type' => 'textfield',
      '#title' => 'Path',
      '#description' => $this->t('Enter a root-relative URL path that begins with a single forward slash. For example: /one/two'),
      '#pattern' => ValidGroupContextPathPrefixConstraintValidator::REGEX,
      '#default_value' => GroupPathPrefix::get($this->entity),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function copyFormValuesToEntity(EntityInterface $entity, array $form, FormStateInterface $form_state): void {
    $values = $form_state->getValues();
    \assert($entity instanceof FieldableEntityInterface);
    $entity->set(GroupPathPrefix::FIELD_NAME, $values[GroupPathPrefix::FIELD_NAME]);
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state): array {
    $actions = parent::actions($form, $form_state);
    unset($actions['delete']);
    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  protected function flagViolations(EntityConstraintViolationListInterface $violations, array $form, FormStateInterface $form_state) {
    foreach ($violations as $violation) {
      $form_state->setErrorByName(\str_replace('.', '][', $violation->getPropertyPath()), $violation->getMessage());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $result = parent::save($form, $form_state);
    $this->messenger()->addStatus($this->t('@label path prefix updated.', [
      '@label' => $this->entity->label(),
    ]));
    $form_state->setRedirect('group_context_path_prefix.overview');
    return (int) $result;
  }

}
