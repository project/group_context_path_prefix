<?php

declare(strict_types=1);

namespace Drupal\group_context_path_prefix;

use Drupal\Core\Config\BootstrapConfigStorageFactory;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceModifierInterface;
use Drupal\Core\Language\LanguageInterface;

/**
 * Defines a service provider for the Group context: Path prefix module.
 */
final class GroupContextPathPrefixServiceProvider implements ServiceModifierInterface {

  private const CONFIG_PREFIX = 'language.entity.';

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container): void {
    // Add the 'url.path' cache context as required.
    $renderer_config = $container->getParameter('renderer.config');
    $renderer_config['required_cache_contexts'][] = 'url.path';
    $container->setParameter('renderer.config', $renderer_config);

    if (!$this->isMultilingual()) {
      // This allows support for URLs like /my-site/my-page. But no support for
      // multilingual. For multilingual, use Language Negotiation:
      // Group Prefix Url.
      $container->getDefinition('group_context_path_prefix.path_processor')
        ->addTag('path_processor_inbound', ['priority' => 250])
        ->addTag('path_processor_outbound', ['priority' => 250]);
    }
    // Remove frontpage path processor as that logic is handled by this module.
    $container->removeDefinition('path_processor_front');
  }

  /**
   * Checks whether the site is multilingual.
   *
   * @return bool
   *   TRUE if the site is multilingual, FALSE otherwise.
   */
  private function isMultilingual(): bool {
    // Assign the prefix to a local variable, so it can be used in an anonymous
    // function.
    $prefix = self::CONFIG_PREFIX;
    $config_storage = BootstrapConfigStorageFactory::get();
    $config_ids = \array_filter($config_storage->listAll($prefix), static fn ($config_id) => $config_id !== $prefix . LanguageInterface::LANGCODE_NOT_SPECIFIED && $config_id !== $prefix . LanguageInterface::LANGCODE_NOT_APPLICABLE);
    return \count($config_ids) > 1;
  }

}
