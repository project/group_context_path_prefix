<?php

declare(strict_types=1);

namespace Drupal\group_context_path_prefix\PathProcessor;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\PathProcessor\InboundPathProcessorInterface;
use Drupal\Core\PathProcessor\OutboundPathProcessorInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\group\Entity\GroupInterface;
use Drupal\group_context_path_prefix\Entity\GroupPathPrefix;
use Drupal\group_context_path_prefix\GroupPathPrefixRepositoryInterface;
use Drupal\group_context_path_prefix\SitePrefixManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Processes paths using group path prefixes.
 */
final class GroupPathPrefixProcessor implements InboundPathProcessorInterface, OutboundPathProcessorInterface {

  /**
   * Array key for modules to pass a specific group to use for outbound links.
   */
  public const LOCALIZED_OPTIONS_KEY = 'group_context_path_prefix';

  /**
   * Flag to skip path prefix processing.
   */
  public const SKIP_PREFIX_PROCESSING = 'group_prefix_skip_prefix_processing';

  /**
   * Constructs a new GroupPathPrefixProcessor object.
   */
  public function __construct(
    private readonly GroupPathPrefixRepositoryInterface $prefixRepository,
    private readonly SitePrefixManagerInterface $sitePrefixManager,
    private readonly ConfigFactoryInterface $configFactory,
    private readonly EntityTypeManagerInterface $entityTypeManager,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function processInbound($path, Request $request): string {
    // Provide this opt-out option so we can use the same path processor on
    // monolingual and multilingual sites.
    if (!$request->attributes->getBoolean(self::SKIP_PREFIX_PROCESSING)) {
      $prefix = $this->sitePrefixManager->getPrefix($path);
      if (!\is_string($prefix)) {
        return $path;
      }
      if ($path !== '/' && !\str_starts_with($path, $prefix)) {
        return $path;
      }
      // Remove the prefix from the path and ensure that the processed path
      // always begins with a forward slash in the case where the prefix exactly
      // matches the inbound path.
      $path = '/' . \ltrim(\substr($path, \strlen($prefix)), '/');
    }

    $path = \parse_url($path, \PHP_URL_PATH);
    if ($path === '/') {
      $path = $this->handleFrontpage($request);
    }

    return $path;
  }

  /**
   * Handle frontpage inbound logic.
   *
   * @see PathProcessorFront::processInbound
   */
  private function handleFrontpage(Request $request): string {
    $path = $this->configFactory->get('system.site')->get('page.front');
    \preg_match('@^/group/(\d+)@', $path, $matches);
    // Handle where groups are the "front-page".
    if (\array_key_exists(1, $matches)) {
      return '/group/' . $this->sitePrefixManager->getGroup()?->id();
    }

    if (empty($path)) {
      // We have to return a valid path but / does not have a route and config
      // might be broken so stop execution.
      throw new NotFoundHttpException();
    }
    $components = \parse_url($path);
    // Remove query string and fragment.
    $path = $components['path'];
    // Merge query parameters from front page configuration value
    // with URL query, so that actual URL takes precedence.
    if (!empty($components['query'])) {
      \parse_str($components['query'], $parameters);
      \array_replace($parameters, $request->query->all());
      $request->query->replace($parameters);
    }

    return $path;
  }

  /**
   * {@inheritdoc}
   */
  public function processOutbound($path, &$options = [], ?Request $request = NULL, ?BubbleableMetadata $bubbleable_metadata = NULL): string {
    $prefix = $this->getPathPrefixIsGroup($path, $options, $bubbleable_metadata);
    if ($prefix !== NULL) {
      return $this->getOutboundPrefix($path, $prefix, $options);
    }
    $prefix = $this->getPathPrefixOverride($options, $bubbleable_metadata);
    if ($prefix !== NULL) {
      return $this->getOutboundPrefix($path, $prefix, $options);
    }
    $prefix = $this->sitePrefixManager->getPrefix($path, $bubbleable_metadata);

    if ($path === '/' && $prefix === NULL) {
      return $this->configFactory->get('system.site')->get('page.front');
    }
    if ($prefix !== NULL) {
      return $this->getOutboundPrefix($path, $prefix, $options);
    }
    $prefix = $this->getGroupPathPrefix($path, $options, $bubbleable_metadata);
    if (!\is_string($prefix)) {
      if (!$request instanceof Request) {
        return $path;
      }
      $prefix = $this->getRequestPathPrefix($request->getPathInfo(), $bubbleable_metadata);
      if (!\is_string($prefix)) {
        return $path;
      }
    }
    return $this->getOutboundPrefix($path, $prefix, $options);
  }

  /**
   * Get the outbound prefix.
   */
  private function getOutboundPrefix(string $path, string $prefix, array &$options = []): string {
    if (!\array_key_exists('prefix', $options)) {
      $options['prefix'] = '';
    }
    $options['prefix'] = \ltrim($prefix, '/') . '/' . $options['prefix'];

    // Handle where groups are the "front-page".
    $front_path = $this->configFactory->get('system.site')->get('page.front');
    \preg_match('@^/group/(\d+)$@', $front_path, $front_matches);
    \preg_match('@^/group/(\d+)$@', $path, $matches);
    if (\array_key_exists(1, $front_matches) && \array_key_exists(1, $matches)) {
      return '/';
    }

    return $path;
  }

  /**
   * Gets the group path prefix from the request path, if it contains one.
   *
   * @param string $path
   *   The request path.
   * @param \Drupal\Core\Render\BubbleableMetadata|null $bubbleable_metadata
   *   The bubbleable metadata.
   *
   * @return string|null
   *   The path prefix or NULL if the request URL does not contain a group path
   *   prefix.
   */
  private function getRequestPathPrefix(string $path, ?BubbleableMetadata $bubbleable_metadata = NULL): ?string {
    $group = $this->prefixRepository->getGroupByRequestPathPrefix($path);
    if (!$group instanceof GroupInterface) {
      return NULL;
    }
    $bubbleable_metadata?->addCacheableDependency($group);
    return GroupPathPrefix::get($group);
  }

  /**
   * Get a path prefix if the URL is a group URL.
   */
  private function getPathPrefixIsGroup(string $path, array &$options, ?BubbleableMetadata $bubbleable_metadata = NULL): ?string {
    \preg_match('@^/group/(\d+)@', $path, $matches);
    if (\array_key_exists(1, $matches)) {
      $group = $this->entityTypeManager->getStorage('group')->load($matches[1]);
      if ($group instanceof GroupInterface) {
        $options[self::LOCALIZED_OPTIONS_KEY]['group'] = $group;
        $bubbleable_metadata?->addCacheableDependency($group);
        return GroupPathPrefix::get($group);
      }
    }
    return NULL;
  }

  /**
   * Get a path prefix override value, if it exists.
   */
  private function getPathPrefixOverride($options = [], ?BubbleableMetadata $bubbleable_metadata = NULL): ?string {
    if (!empty($options[self::LOCALIZED_OPTIONS_KEY]['group'])) {
      $group = $options[self::LOCALIZED_OPTIONS_KEY]['group'];
      \assert($group instanceof GroupInterface);
      $bubbleable_metadata?->addCacheableDependency($group);
      return GroupPathPrefix::get($group);
    }
    return NULL;
  }

  /**
   * Gets a group path prefix if the path identifies an entity in a group.
   *
   * @param string $path
   *   The path to check.
   * @param array $options
   *   An associative array of additional options provided by path processing.
   * @param \Drupal\Core\Render\BubbleableMetadata|null $bubbleable_metadata
   *   (optional) A BubbleableMetadata object.
   *
   * @return string|null
   *   The path prefix or NULL if the path does not have one.
   */
  private function getGroupPathPrefix(string $path, array $options, ?BubbleableMetadata $bubbleable_metadata = NULL): ?string {
    $group = $this->prefixRepository->getGroupByInternalPath($path, $bubbleable_metadata);
    if (!$group instanceof GroupInterface) {
      return NULL;
    }
    $bubbleable_metadata?->addCacheableDependency($group);
    return GroupPathPrefix::get($group);
  }

}
