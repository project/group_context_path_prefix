<?php

declare(strict_types=1);

namespace Drupal\group_context_path_prefix;

use Drupal\Core\Render\BubbleableMetadata;
use Drupal\group\Entity\GroupInterface;

/**
 * Site prefix manager interface.
 */
interface SitePrefixManagerInterface {

  /**
   * Initialize the group that contains a path prefix.
   */
  public function initPrefixGroup(string $path): void;

  /**
   * Get the group path prefix.
   */
  public function getPrefix(string $path, ?BubbleableMetadata $bubbleable_metadata = NULL): ?string;

  /**
   * Get the group.
   */
  public function getGroup(): ?GroupInterface;

}
