<?php

declare(strict_types=1);

namespace Drupal\group_context_path_prefix\EventSubscriber;

use Drupal\Core\Routing\CacheableRouteProviderInterface;
use Drupal\Core\Routing\RouteProviderInterface;
use Drupal\group_context_path_prefix\SitePrefixManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Initializes the group path prefix.
 */
final class GroupPathPrefixSubscriber implements EventSubscriberInterface {

  /**
   * Constructs a GroupPathPrefixSubscriber object.
   */
  public function __construct(
    private readonly SitePrefixManagerInterface $sitePrefixManager,
    private readonly RouteProviderInterface $routeProvider,
  ) {}

  /**
   * Initializes the group path prefix manager at the beginning of the request.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The Event to process.
   */
  public function onKernelRequestGroupPathPrefix(RequestEvent $event): void {
    if ($event->isMainRequest()) {
      $requestPath = $event->getRequest()->getPathInfo();
      $this->initPathPrefix($requestPath);
      if ($this->routeProvider instanceof CacheableRouteProviderInterface) {
        $prefix = $this->sitePrefixManager->getPrefix($requestPath);
        if (\is_string($prefix)) {
          $this->routeProvider->addExtraCacheKeyPart('group_context_path_prefix', $prefix);
        }
      }
    }
  }

  /**
   * Initializes the path prefix in the site prefix manager.
   */
  private function initPathPrefix(string $path): void {
    $this->sitePrefixManager->initPrefixGroup($path);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events = [];
    $events[KernelEvents::REQUEST][] = ['onKernelRequestGroupPathPrefix', 256];
    return $events;
  }

}
