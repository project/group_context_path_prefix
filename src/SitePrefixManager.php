<?php

declare(strict_types=1);

namespace Drupal\group_context_path_prefix;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupInterface;
use Drupal\group_context_path_prefix\Entity\GroupPathPrefix;

/**
 * Provide the path prefix for a URL.
 */
final class SitePrefixManager implements SitePrefixManagerInterface {

  /**
   * The group.
   */
  private ?GroupInterface $group = NULL;

  /**
   * Constructs a SitePrefixManager object.
   */
  public function __construct(
    private readonly GroupPathPrefixRepositoryInterface $prefixRepository,
    private readonly ConfigFactoryInterface $configFactory,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function initPrefixGroup(string $path): void {
    $path = \parse_url($path, \PHP_URL_PATH);
    if ($path === '/') {
      $path = $this->configFactory->get('system.site')->get('page.front');
    }
    // Guard against seriously malformed URLs.
    if (!\is_string($path)) {
      return;
    }
    $this->group = $this->prefixRepository->getGroupByRequestPathPrefix($path);
    if ($this->group === NULL) {
      \preg_match('@^/group/(\d+)@', $path, $matches);
      if (\array_key_exists(1, $matches)) {
        $this->group = Group::load($matches[1]);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getPrefix(string $path, ?BubbleableMetadata $bubbleable_metadata = NULL): ?string {
    if (empty($this->group)) {
      $this->initPrefixGroup($path);
    }
    if (empty($this->group)) {
      return NULL;
    }
    $bubbleable_metadata?->addCacheableDependency($this->group);
    return GroupPathPrefix::get($this->group);
  }

  /**
   * {@inheritdoc}
   */
  public function getGroup(): ?GroupInterface {
    return $this->group;
  }

}
