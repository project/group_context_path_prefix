<?php

declare(strict_types=1);

namespace Drupal\group_context_path_prefix\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityListBuilderInterface;
use Drupal\group_context_path_prefix\GroupContextPathPrefixListBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an overview of groups and their path prefixes.
 */
final class GroupContextPathPrefixOverview extends ControllerBase {

  /**
   * Constructs a new GroupContextPathPrefixOverview object.
   *
   * @param \Drupal\Core\Entity\EntityListBuilderInterface $groupPathPrefixListBuilder
   *   A list builder which renders a table displaying each group's path prefix.
   */
  public function __construct(
    private readonly EntityListBuilderInterface $groupPathPrefixListBuilder,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    $group_entity_type = $container->get('entity_type.manager')->getDefinition('group');
    return new self(GroupContextPathPrefixListBuilder::createInstance($container, $group_entity_type));
  }

  /**
   * Builds the overview.
   */
  public function __invoke(): array {
    return $this->groupPathPrefixListBuilder->render();
  }

}
