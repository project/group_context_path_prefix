<?php

declare(strict_types=1);

namespace Drupal\group_context_path_prefix\Plugin\LanguageNegotiation;

use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\PathProcessor\InboundPathProcessorInterface;
use Drupal\Core\PathProcessor\OutboundPathProcessorInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\group_context_path_prefix\PathProcessor\GroupPathPrefixProcessor;
use Drupal\language\Attribute\LanguageNegotiation;
use Drupal\language\LanguageNegotiationMethodBase;
use Drupal\language\LanguageSwitcherInterface;
use Drupal\language\Plugin\LanguageNegotiation\LanguageNegotiationUrl;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class for identifying language via URL prefix.
 */
#[LanguageNegotiation(
  id: GroupPrefixLanguageNegotiationUrl::METHOD_ID,
  name: new TranslatableMarkup('Group Prefix URL'),
  types: [LanguageInterface::TYPE_INTERFACE,
    LanguageInterface::TYPE_CONTENT,
    LanguageInterface::TYPE_URL,
  ],
  weight: -9,
  description: new TranslatableMarkup("Language from the URL (Path prefix or domain) with Group Path Prefix support."),
  config_route_name: 'language.negotiation_url'
)]
class GroupPrefixLanguageNegotiationUrl extends LanguageNegotiationMethodBase implements InboundPathProcessorInterface, OutboundPathProcessorInterface, LanguageSwitcherInterface, ContainerFactoryPluginInterface {

  /**
   * The language negotiation method id.
   */
  public const METHOD_ID = 'group-prefix-language-url';

  public function __construct(
    private readonly GroupPathPrefixProcessor $groupPathPrefixProcessor,
    private readonly LanguageNegotiationUrl $languageNegotiationUrl,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('group_context_path_prefix.path_processor'),
      $container->get('language_negotiator')->getNegotiationMethodInstance(LanguageNegotiationUrl::METHOD_ID),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getLangcode(?Request $request = NULL) {
    $langCode = NULL;
    if ($request !== NULL && $this->languageManager instanceof LanguageManagerInterface) {
      $languages = $this->languageManager->getLanguages();
      $config = $this->config->get('language.negotiation')->get('url');

      $langCode = match ($config['source']) {
        LanguageNegotiationUrl::CONFIG_PATH_PREFIX => $this->pathPrefix($request, $languages, $config),
        LanguageNegotiationUrl::CONFIG_DOMAIN => $this->domain($request, $languages, $config),
      };
    }
    return $langCode;

  }

  /**
   * Handle path prefix language negotiation.
   */
  private function pathPrefix(Request $request, array $languages, array $config): ?string {
    $request_path = \urldecode(\trim($this->groupPathPrefixProcessor->processInbound($request->getPathInfo(), $request), '/'));
    $path_args = \explode('/', $request_path);
    $prefix = \array_shift($path_args);

    // Search prefix within added languages.
    $negotiated_language = FALSE;
    foreach ($languages as $language) {
      if (isset($config['prefixes'][$language->getId()]) && $config['prefixes'][$language->getId()] === $prefix) {
        $negotiated_language = $language;
        break;
      }
    }

    if ($negotiated_language !== FALSE) {
      return $negotiated_language->getId();
    }
    return NULL;
  }

  /**
   * Handle domain language negotiation.
   */
  private function domain(Request $request, array $languages, array $config): ?string {
    // Get only the host, not the port.
    $http_host = $request->getHost();
    foreach ($languages as $language) {
      // Skip the check if the language doesn't have a domain.
      if (!empty($config['domains'][$language->getId()])) {
        // Ensure that there is exactly one protocol in the URL when
        // checking the hostname.
        $host = 'https://' . \str_replace([
          'http://',
          'https://',
        ], '', $config['domains'][$language->getId()]);
        $host = \parse_url($host, \PHP_URL_HOST);
        if ($http_host === $host) {
          return $language->getId();
        }
      }
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function processInbound($path, Request $request): string {
    $path = $this->groupPathPrefixProcessor->processInbound($path, $request);
    $path = $this->languageNegotiationUrl->processInbound($path, $request);
    // Call a second time to handle front-page logic. Do this fancy logic so
    // the same path processor can be used on both multilingual and monolingual
    // websites.
    $request->attributes->set(GroupPathPrefixProcessor::SKIP_PREFIX_PROCESSING, TRUE);
    $path = $this->groupPathPrefixProcessor->processInbound($path, $request);
    $request->attributes->remove(GroupPathPrefixProcessor::SKIP_PREFIX_PROCESSING);
    return $path;
  }

  /**
   * {@inheritdoc}
   */
  public function processOutbound($path, &$options = [], ?Request $request = NULL, ?BubbleableMetadata $bubbleable_metadata = NULL): string {
    $path = $this->languageNegotiationUrl->processOutbound($path, $options, $request, $bubbleable_metadata);
    $path = $this->groupPathPrefixProcessor->processOutbound($path, $options, $request, $bubbleable_metadata);
    return $path;
  }

  /**
   * {@inheritdoc}
   */
  public function getLanguageSwitchLinks(Request $request, $type, Url $url): array {
    $links = [];
    $query = [];
    \parse_str($request->getQueryString() ?? '', $query);

    foreach ($this->languageManager->getNativeLanguages() as $language) {
      $links[$language->getId()] = [
        // We need to clone the $url object to avoid using the same one for all
        // links. When the links are rendered, options are set on the $url
        // object, so if we use the same one, they would be set for all links.
        'url' => clone $url,
        'title' => $language->getName(),
        'language' => $language,
        'attributes' => ['class' => ['language-link']],
        'query' => $query,
      ];
    }

    return $links;
  }

}
