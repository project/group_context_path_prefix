<?php

declare(strict_types=1);

namespace Drupal\group_context_path_prefix\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Provides a Valid path prefix. constraint.
 *
 * @Constraint(
 *   id = "ValidGroupContextPathPrefix",
 *   label = @Translation("Valid path prefix.", context = "Validation"),
 * )
 */
final class ValidGroupContextPathPrefixConstraint extends Constraint {

  /**
   * The default violation message.
   */
  public string $message = '%value is not a valid URL path prefix.';

  /**
   * Returns the name of the class that validates this constraint.
   *
   * @return string
   *   The constraint validator.
   */
  public function validatedBy(): string {
    return ValidGroupContextPathPrefixConstraintValidator::class;
  }

}
