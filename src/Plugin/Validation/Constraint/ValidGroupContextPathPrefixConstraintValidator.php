<?php

declare(strict_types=1);

namespace Drupal\group_context_path_prefix\Plugin\Validation\Constraint;

use Drupal\Component\Utility\UrlHelper;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the Valid path prefix. constraint.
 */
final class ValidGroupContextPathPrefixConstraintValidator extends ConstraintValidator {

  /**
   * Matches valid path prefix strings.
   *
   * Only matches paths with a leading forward slash, no backslashes, no
   * trailing slash, and no query string (?) or fragment (#) control characters.
   */
  public const REGEX = '^\/([^\/\\?#]*)*(\/[^\/\\?#]+)*$';

  /**
   * {@inheritdoc}
   */
  public function validate(mixed $value, Constraint $constraint): void {
    if (UrlHelper::isValid($value)) {
      $url = UrlHelper::parse($value);
      if (empty($url['query']) && empty($url['fragment'])) {
        if (\preg_match(\sprintf('/%s/', self::REGEX), $value) !== 0) {
          // Fully validated.
          return;
        }
      }
    }
    $this->context->buildViolation($constraint->message)
      ->setParameter('%value', $value)
      ->addViolation();
  }

}
