<?php

declare(strict_types=1);

namespace Drupal\group_context_path_prefix;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Render\Markup;
use Drupal\group\Entity\GroupInterface;
use Drupal\group_context_path_prefix\Entity\GroupPathPrefix;

/**
 * Provides a list of groups and their path prefixes.
 */
final class GroupContextPathPrefixListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  protected $entityTypeId = 'group';

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    return [
      'group' => $this->t('Group'),
      'path_prefix' => $this->t('Path prefix'),
    ] + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    \assert($entity instanceof GroupInterface);
    $path_prefix = GroupPathPrefix::get($entity);
    return [
      'group' => [
        'data' => $entity->label(),
      ],
      'path_prefix' => [
        'data' => \is_null($path_prefix)
          ? $this->t('<i>Not defined</i>')
          : Markup::create('<code>' . $path_prefix . '</code>'),
      ],
    ] + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getOperations(EntityInterface $entity): array {
    return [
      'edit' => [
        'title' => $this->t('Edit path'),
        'url' => $entity->toUrl('edit-path-prefix-form'),
      ],
    ];
  }

}
